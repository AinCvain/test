﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class SpeedSlider : MonoBehaviour
{

    public CharacterController characterController;
    private Slider speedSlider;
    public Text speedValue;

    void Start()
    {
        speedSlider = GetComponent <Slider>();
        SetSpeed();
    }

    public void SetSpeed()
    {
        characterController.SetSpeed(speedSlider.value);
        speedValue.text = speedSlider.value.ToString();
    }
    
}
