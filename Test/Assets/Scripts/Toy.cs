﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Toy : MonoBehaviour
{
    private Rigidbody rigidBody;
    private Material material;

    public bool red, green, blue;

    private void Start()
    {
        rigidBody = GetComponent<Rigidbody>();
        material = GetComponent<Renderer>().material;
        redValue = 0;
        greenValue = 0;
        blueValue = 0;
    }

    float redValue;
    float greenValue;
    float blueValue;
    void Update()
    {
        if (red)
        {
            redValue = Mathf.Abs(rigidBody.velocity.x + rigidBody.velocity.z + rigidBody.velocity.y) / 10;
        }
        if (green)
        {
            greenValue = Mathf.Abs(rigidBody.velocity.x + rigidBody.velocity.z + rigidBody.velocity.y) / 10;
        }
        if (blue)
        {
            blueValue = Mathf.Abs(rigidBody.velocity.x + rigidBody.velocity.z + rigidBody.velocity.y) / 10;
        }
        material.color = new Color(redValue, greenValue, blueValue);
    }
}
