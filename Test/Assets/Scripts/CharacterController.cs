﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

struct MovePoint
{
    public float x;
    public float z;
    public MovePoint(float x, float z)
    {
        this.x = x;
        this.z = z;
    }
}

enum MouseButtons { LeftKey }

public class CharacterController : MonoBehaviour
{

    public float jumpForce = 500.0f;
    public GameObject movePoint;
    private float speed = 0.0f;
    private CharacterController characterController;
    private List<MovePoint> stackMovePoints;
    private Rigidbody rigidBody;
    private bool onFloor = false;

    private void Start()
    {
        characterController = GetComponent<CharacterController>();
        rigidBody = GetComponent<Rigidbody>();
        stackMovePoints = new List<MovePoint>();
    }

    
    private void Update()
    {
        if (Input.GetKey(KeyCode.LeftShift) && Input.GetMouseButtonDown((int)MouseButtons.LeftKey))
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit) && hit.collider.gameObject.layer != 5)
            {
                addMovePoint(hit.point.x, hit.point.z);
            }
        }
        else if (Input.GetMouseButtonDown((int)MouseButtons.LeftKey))
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit) && hit.collider.gameObject.layer != 5)
            {
                deleteAllMovePoints();
                addMovePoint(hit.point.x, hit.point.z);
            }
        }
        if (Input.GetKeyDown(KeyCode.Space))
        {
            Jump();
        }
    }

    private void FixedUpdate()
    {
        moveAroundPoints();
    }

    private void moveAroundPoints()
    {
        if (stackMovePoints.Count > 0 && onFloor)
        {
            float deltaX = stackMovePoints[0].x - transform.position.x;
            float deltaZ = stackMovePoints[0].z - transform.position.z;

            Vector3 moveVector = new Vector3(deltaX, 0, deltaZ);

            moveVector = moveVector.normalized * speed;

            rigidBody.velocity = new Vector3(moveVector.x, rigidBody.velocity.y, moveVector.z);

            if (Mathf.RoundToInt(deltaX) == 0 && Mathf.RoundToInt(deltaZ) == 0)
            {
                rigidBody.velocity = Vector3.zero;
                stackMovePoints.RemoveAt(0);
            }
        }
    }

    private void addMovePoint(float x, float z)
    {
        stackMovePoints.Add(new MovePoint(x, z));
    }

    private void deleteAllMovePoints()
    {
        stackMovePoints.Clear();
    }

    private void Jump()
    {
        if (onFloor)
        {
            rigidBody.AddForce(new Vector3(rigidBody.velocity.x, jumpForce, rigidBody.velocity.z), ForceMode.Impulse);
        }
    }

    private void OnCollisionStay(Collision collision)
    {
        if (collision.gameObject.layer == 8)
        {
            onFloor = true;
        }
    }
    private void OnCollisionExit(Collision collision)
    {
        if (collision.gameObject.layer == 8)
        {
            onFloor = false;
        }
    }

    public void SetSpeed(float speed)
    {
        this.speed = speed;
    }
}
